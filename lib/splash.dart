import 'dart:async';
import 'dart:convert';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:qiaShop/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:qiaShop/views/admin/pages/menu.dart';
import 'package:qiaShop/views/pages/login.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'views/pages/menu.dart';

class OPSplashScreen extends StatefulWidget {
  final bool tandaUpdate;
  static String tag = '/OPSplashScreen';

  OPSplashScreen({this.tandaUpdate});
  @override
  _OPSplashScreenState createState() => _OPSplashScreenState();
}

class _OPSplashScreenState extends State<OPSplashScreen>
    with SingleTickerProviderStateMixin {
  String kode;
  String tokens;
  bool loginStatus;
  bool statusTanda;
  BlocLoginBloc bloc = BlocLoginBloc();
  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohome);
  }

  startTimeUser() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohomeUser);
  }

  startTimeLogin() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statuslogin);
  }

  @override
  void initState() {
    super.initState();
    bloc..add(BlocCekToken());
    OneSignal.shared.setNotificationReceivedHandler((notification) {
      
     }); 
  }

  void navigationPage()async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Login(),
      ),
    );
  }

  Future<String> statuslogin() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Login(),
      ),
    );
  }

  Future<String> statustohome() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BackgroundMenuAdmin(page: 0,),
      ),
    );
  }

  Future<String> statustohomeUser() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Menu(page: 0,),
      ),
    );
  }

  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        body: BlocListener<BlocLoginBloc, BlocLoginState>(
          bloc: bloc,
          listener: (context, state) {
            // TODO: implement listener
            if(state is BlocStateCekTokenSukses){
              if(state.data.role == 1){
                startTime(); //jika role admin maka ke page admin
              } else {
                startTimeUser(); //jika role user maka ke page user
              }
            }
            if(state is BlocaStateCekTokenFailed){
              startTimeLogin();
            }
          },
          child: Container(
            child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
              bloc:bloc,
              builder: (context, state) {
                if(state is BlocStateCekTokenSukses){
                  return myBody(size);
                }
                if(state is BlocStateLoading){
                  return myBody(size);
                }
                if(state is BlocaStateCekTokenFailed){
                  return myBody(size);
                }
                return myBody(size);
              },
            ),
          ),
        )
    );
  }

  Container myBody(Size size) {
    return Container(
      width: size.width,
      height: size.height,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [circlePurpleDark, background1],
              begin: Alignment.topRight,
              end: Alignment.bottomRight)),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Image.asset("assets/images/leaf.png", width: 100),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Q",
                style: TextStyle(
                    fontSize: 45,
                    fontFamily: 'RobotoCondensed',
                    color: Colors.white),
              ),
              Text("ia",
                  style: TextStyle(
                      fontSize: 25,
                      fontFamily: 'RobotoCondensed',
                      color: Colors.white))
            ],
          )
        ],
      ),
    );
  }
}
