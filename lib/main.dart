import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:qiaShop/splash.dart';
import 'package:qiaShop/views/pages/login.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'dart:async';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  OneSignal.shared.init("472c7fd8-7ac4-4b0f-8114-f46ce747c53e", iOSSettings: null);
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  runApp(MaterialApp(
    home: OPSplashScreen(),
    debugShowCheckedModeBanner: false,
  ));
}
