part of 'barang_bloc.dart';

@immutable
abstract class BarangEvent {}

class EventGetBarang extends BarangEvent {
  
}

class EventPostBarang extends BarangEvent{
  BarangModel data;
  EventPostBarang({this.data});
}

class EventPutBarang extends BarangEvent{
  BarangModel data;
  EventPutBarang({this.data});
}

class EventDelBarang extends BarangEvent{
  BarangModel data;
  EventDelBarang({this.data});
}

class EventGetBarangRekom extends BarangEvent{
  
}

class EventGetBarangPromo extends BarangEvent{
  
}