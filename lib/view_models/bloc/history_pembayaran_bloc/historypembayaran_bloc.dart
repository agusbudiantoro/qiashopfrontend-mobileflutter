import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:qiaShop/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:qiaShop/view_models/networkApi/historyPembayaran/historyPembayaranApi.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'historypembayaran_event.dart';
part 'historypembayaran_state.dart';

class HistorypembayaranBloc extends Bloc<HistorypembayaranEvent, HistorypembayaranState> {
  HistorypembayaranBloc() : super(HistorypembayaranInitial());

  @override
  Stream<HistorypembayaranState> mapEventToState(
    HistorypembayaranEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventGetHistoryPembayaran){
      yield* _getHistoryPembayaran();
    }
  }
}

Stream<HistorypembayaranState> _getHistoryPembayaran()async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield StateHistoryPembayaranWaiting();
  try {
    int id = await preferences.getInt('myId');
    dynamic value = await HistoryPembayaran.getHistoryPembayaranByIdKonsumen(id);
    var hasil = jsonDecode(value);
    ModelGetPemesanan hasilConvert = ModelGetPemesanan.fromJson(hasil);
    List<ValueGetPemesanan> hasilGetHistoryPembayaran = hasilConvert.value;
    yield StateHistoryPembayaranSukses(data: hasilGetHistoryPembayaran);
  } catch (e) {
    yield StateHistoryPembayaranFailed(errorMessage: e.toString());
  }
}