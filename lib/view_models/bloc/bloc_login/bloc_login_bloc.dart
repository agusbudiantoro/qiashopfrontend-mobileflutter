import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:qiaShop/models/login/model_form_login.dart';
import 'package:qiaShop/models/login/model_login.dart';
import 'package:qiaShop/view_models/networkApi/login/loginApi.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_login_event.dart';
part 'bloc_login_state.dart';

class BlocLoginBloc extends Bloc<BlocLoginEvent, BlocLoginState> {
  BlocLoginBloc() : super(BlocLoginInitial());

  @override
  Stream<BlocLoginState> mapEventToState(
    BlocLoginEvent event,
  ) async* {
    if(event is BlocEventLogin){
      yield* _login(event.myData);
    }
    if(event is BlocCekToken){
      yield* _cekToken();
    }
    if(event is BlocRegister){
      yield* _register(event.data);
    }
  }
}

Stream<BlocLoginState> _register(data)async*{
  yield BlocStateRegistLoading();
  try {
    dynamic value = await LoginApi.registerAkun(data);
    yield BlocStateRegistSukses();
  } catch (e) {
    yield BlocaStateRegistFailed(errorMessage: e.toString());
  }
}

Stream<BlocLoginState> _cekToken()async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoading();
  try {
    String token = await preferences.getString("token");
    if(token != null){
      dynamic value = await LoginApi.cekTokenLogin(token);
      var hasil = jsonDecode(value);
      ValueDataLogin hasilConvert = ValueDataLogin.fromJson(hasil);
      MyDataLogin hasilFinal = hasilConvert.value;
      yield BlocStateCekTokenSukses(data: hasilFinal);
    } else {
      yield BlocaStateCekTokenFailed();
    }
  } catch (e) {
    yield BlocaStateCekTokenFailed();
  }
}

Stream<BlocLoginState> _login(data)async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoading();
  try {
      dynamic value = await LoginApi.loginAkun(data);
      var hasil = jsonDecode(value);
      MyDataLogin hasilConvert = MyDataLogin.fromJson(hasil);
      await preferences.setString("username", hasilConvert.userName);
      await preferences.setString("token", hasilConvert.token);
      await preferences.setInt("myId", hasilConvert.currUser);
      await preferences.setInt("myRole", hasilConvert.role);
      await preferences.setString('myFoto', hasilConvert.foto);
      await preferences.setString("noHp", hasilConvert.no_hp);
      await preferences.setString("myAlamat", hasilConvert.alamat);
      await preferences.setString("myEmail", hasilConvert.email);
    yield BlocStateSukses(myData: hasilConvert);
  } catch (e) {
    yield BlocaStateFailed(errorMessage: "gagal login");
  }
}