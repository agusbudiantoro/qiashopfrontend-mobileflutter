part of 'barangkeranjang_bloc.dart';

@immutable
abstract class BarangkeranjangState {}

class BarangkeranjangInitial extends BarangkeranjangState {}

// post barrang to keranjang
class StatePostBarangKeranjangSukses extends BarangkeranjangState {
}

class StatePostBarangKeranjangWaiting extends BarangkeranjangState{
  
}

class StatePostBarangKeranjangFailed extends BarangkeranjangState {
  String errorMessage;
  StatePostBarangKeranjangFailed({this.errorMessage});
}

// get barrang to keranjang
class StateGetBarangKeranjangByIdKonsumenSukses extends BarangkeranjangState {
  List<ValuesListBarangKeranjang> data;
  StateGetBarangKeranjangByIdKonsumenSukses({this.data});
}

class StateGetBarangKeranjangByIdKonsumenWaiting extends BarangkeranjangState{
  
}

class StateGetBarangKeranjangByIdKonsumenFailed extends BarangkeranjangState {
  String errorMessage;
  StateGetBarangKeranjangByIdKonsumenFailed({this.errorMessage});
}

// delete barang keranjang
class StateDelBarangKeranjangSukses extends BarangkeranjangState {
  
}

class StateDelBarangKeranjangWaiting extends BarangkeranjangState{
  
}

class StateDelBarangKeranjangFailed extends BarangkeranjangState {
  String errorMessage;
  StateDelBarangKeranjangFailed({this.errorMessage});
}