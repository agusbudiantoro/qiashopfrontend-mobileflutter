import 'dart:io';

import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:qiaShop/models/keranjang/formKeranjang.dart';


const urlGetPembayaran = "https://qiaminimarketjaksel.com/auth/api/v2/getPesananByIdKonsumen";

class HistoryPembayaran {
  static Future<dynamic> getHistoryPembayaranByIdKonsumen(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'id_konsumen':id.toString()
    };
      var response = await ioClient.post(Uri.parse(urlGetPembayaran), body: body);
      print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.message.toString());
    }
  }
}