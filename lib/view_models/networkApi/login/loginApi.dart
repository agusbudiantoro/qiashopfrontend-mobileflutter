import 'dart:io';

import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
// import 'package:http/http.dart';
import 'package:qiaShop/models/login/model_form_login.dart';
import 'package:qiaShop/models/login/model_login.dart';
// import 'package:qiaShop/models/login/model_login.dart';

const urlLogin = "https://qiaminimarketjaksel.com/auth/api/v1/login";
const urlCekToken = "https://qiaminimarketjaksel.com/auth/api/v1/cekOauthToken";
const urlRegister = "https://qiaminimarketjaksel.com/auth/api/v1/register";

class LoginApi {
  static Future<dynamic> registerAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    MyDataFormLogin convertData = data;
    Map<String, String>body={
      'email':convertData.email.toString(),
      'password':convertData.password.toString(),
      'username':convertData.username.toString(),
      'role':'2',
      'no_hp':convertData.noHp.toString(),
      'alamat':convertData.alamat.toString()
    };
    try {
      var response = await ioClient.post(Uri.parse(urlRegister), body: body);
      return response.body;
    } catch (e) {
      print(e.toString());
    }
  }

  static Future<dynamic> cekTokenLogin(token)async{
    HttpClient client1 = new HttpClient();
    client1.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client1);
    try {
      var response = await ioClient.post(Uri.parse(urlCekToken),headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
        },);
      if(response.statusCode == 200){
        return response.body;
      }else {
        return throw Exception("Data tidak ditemukan");
      }
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> loginAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    MyDataFormLogin convertData = data;
    Map<String, String>body={
      'email':convertData.email,
      'password':convertData.password
    };
    try {
      var response = await ioClient.post(Uri.parse(urlLogin), body: body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}