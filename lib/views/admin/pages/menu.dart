import 'package:flutter/material.dart';
import 'package:qiaShop/views/admin/pages/pesanan/daftarPesanan.dart';
import 'package:qiaShop/views/admin/widgets/menu/menuAdmin.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_buttonNavigationBar.dart';
import 'package:qiaShop/views/widgets/home/background.dart';
import 'package:qiaShop/views/widgets/keranjang/backgroundKeranjang.dart';
import 'package:qiaShop/views/widgets/pembayaran/historiPembayaran.dart';
import 'package:qiaShop/views/widgets/profile/profile.dart';

import 'home.dart';
import 'kelolaBarang/indexListBarang.dart';

class BackgroundMenuAdmin extends StatefulWidget {
  int page;
  BackgroundMenuAdmin({this.page});
  @override
  _BackgroundMenuAdminState createState() => _BackgroundMenuAdminState(page:this.page);
}

class _BackgroundMenuAdminState extends State<BackgroundMenuAdmin> {
  _BackgroundMenuAdminState({this.page});
  bool statusPass = false;
  int page;
  // BlocLoginBloc bloc = BlocLoginBloc();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Home',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
      body: (page == 0)?BackgroundHomeAdmin():(page == 1)?DaftarPesanan():(page == 2)?IndexListBarang(jenis: 3,):(page == 3)?BackgroundProfil():Container(),
      bottomNavigationBar: BottomBarHomeAdmin(
        page:page,
        selectPage: (int val){
        setState(() {
          page=val;
        });
      },),
    ),
    );
  }
}
