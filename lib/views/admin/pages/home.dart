import 'package:flutter/material.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/models/total/model_total.dart';
import 'package:qiaShop/views/pages/detailProduk.dart';
import 'package:qiaShop/views/pages/feeds.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/content/images.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_container.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_container_child.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_search.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_sphare.dart';
import 'package:qiaShop/views/utils/data/content.dart';

class BackgroundHomeAdmin extends StatefulWidget {
  @override
  _BackgroundHomeAdminState createState() => _BackgroundHomeAdminState();
}

class _BackgroundHomeAdminState extends State<BackgroundHomeAdmin> {
  bool statusPass = false;
  List<JenisTotal> listJenisTotal;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listJenisTotal = jenisTotalAtHome();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [circlePurpleDark, background1],
                begin: Alignment.topRight,
                end: Alignment.bottomRight)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: size.height / 4.5,
                padding: EdgeInsets.only(bottom: 0),
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      child: SphareHome(
                        height: size.height / 5,
                        width: size.width,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0),
                child: ContainerHome(
                    height: size.height / 6,
                    width: size.width,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                              child: ListView.builder(
                                  itemCount: listJenisTotal.length,
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            // Navigator.pop(context, true);
                                          },
                                          child: ContainerHomeChild(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  padding: EdgeInsets.all(5),
                                                  alignment: Alignment.center,
                                                  width: size.width,
                                                  height: size.height / 20,
                                                  child: Text(listJenisTotal[index].namaTotal, style: TextStyle(color: Colors.white, fontFamily: "RobotoCondensed", fontWeight: FontWeight.bold, fontSize: 15),)
                                                ),
                                                Container(
                                                  child: Column(
                                                    children: [
                                                      Text(listJenisTotal[index].value.toString(),
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            height: size.height / 10,
                                            width: size.width / 2,
                                          ),
                                        ),
                                      ],
                                    );
                                  })),
                        ],
                      ),
                    )),
              ),              
            ],
          ),
        ),
      ),
    );
  }
}
