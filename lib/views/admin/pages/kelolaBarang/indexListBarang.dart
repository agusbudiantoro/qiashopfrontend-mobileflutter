import 'package:flutter/material.dart';
import 'package:qiaShop/views/widgets/isiFeed/feed.dart';
import 'package:qiaShop/views/widgets/keranjang/backgroundKeranjang.dart';

import 'addBarang.dart';
import 'listBarang.dart';


class IndexListBarang extends StatefulWidget {
  final int jenis;

  IndexListBarang({this.jenis});
  @override
  _IndexListBarangState createState() => _IndexListBarangState();
}

class _IndexListBarangState extends State<IndexListBarang> {
  TextEditingController cariBarang= TextEditingController();
  String setCariBarang="";

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        actions: [
          Container(
            padding: EdgeInsets.only(top:5, bottom: 10),
            // color: Colors.white,
            width: size.width / 1.35,
            child: TextField(
              controller: cariBarang,
              style: TextStyle(
                fontSize: 14
              ),
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                border: OutlineInputBorder(),
                labelText: 'Cari Barang',
              ),
            ),
          ),
          Container(
            child: IconButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  setCariBarang = cariBarang.text.toString();
                });
              },
              icon: Icon(Icons.search_sharp),
            ),
          ),
          Container(
            child: IconButton(
              color: Colors.white,
              onPressed: () {
                Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddBarang()));
              },
              icon: Icon(Icons.add_box_outlined),
            ),
          )
        ],
        automaticallyImplyLeading: false,
      ),
      body: ListBarang(jenis: widget.jenis, searchBarang: setCariBarang.toString(),)
    );
  }
}
