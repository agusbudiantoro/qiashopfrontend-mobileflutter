import 'package:flutter/material.dart';
import 'package:qiaShop/views/utils/colors.dart';

class BottomBarHomeAdmin extends StatefulWidget {
  final Function(int) selectPage;
  final int page;
  BottomBarHomeAdmin({this.selectPage, this.page});
  @override
  _BottomBarHomeAdminState createState() => _BottomBarHomeAdminState();
}

class _BottomBarHomeAdminState extends State<BottomBarHomeAdmin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left:20,right:20),
      height: 62,
      decoration: BoxDecoration(color: background1, boxShadow: [
        BoxShadow(
            offset: Offset(0, -10),
            blurRadius: 35,
            color: Colors.white.withOpacity(0.30))
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
              children: [
                IconButton(icon: Icon((widget.page == 0)?Icons.home:Icons.home_outlined, color: Colors.white,), iconSize: 30, onPressed: () {
                  widget.selectPage(0);
                }),
                Text("Beranda", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 0)?FontWeight.bold:FontWeight.normal),)
              ],
            ),
          Column(
            children: [
              IconButton(icon: Icon((widget.page == 1)?Icons.shopping_cart_rounded:Icons.shopping_cart_outlined,color: Colors.white), iconSize: 30, onPressed: () {
                widget.selectPage(1);
              }),
              Text("Daftar Pesanan", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 1)?FontWeight.bold:FontWeight.normal),)
            ],
          ),
          Column(
            children: [
              IconButton(icon: Icon((widget.page == 2)?Icons.assessment:Icons.assessment_outlined,color: Colors.white,), iconSize: 30, onPressed: () {
                widget.selectPage(2);
              }),
              Text("Kelola Barang", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 2)?FontWeight.bold:FontWeight.normal),)
            ],
          ),
          Column(
            children: [
              IconButton(icon: Icon((widget.page == 3)?Icons.people_alt:Icons.people_alt_outlined,color: Colors.white,), iconSize: 30, onPressed: () {
                widget.selectPage(3);
              }),
              Text("Profil", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 3)?FontWeight.bold:FontWeight.normal),)
            ],
          ),
        ],
      ),
    );
  }
}
