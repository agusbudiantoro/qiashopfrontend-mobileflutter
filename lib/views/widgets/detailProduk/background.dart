import 'package:flutter/material.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/custom_widgets/detailProduk/custom_container.dart';

import 'modal.dart';

class BackgroundDetailProduk extends StatefulWidget {
  final String images;
  final String harga;
  final String hargaPromo;
  final String diskon;
  final String namaBarang;
  final String deskripsi;
  final int qty;
  final int idBarang;

  BackgroundDetailProduk(
      {this.diskon,
      this.harga,
      this.hargaPromo,
      this.images,
      this.namaBarang,
      this.deskripsi,
      this.qty,
      this.idBarang});
  @override
  _BackgroundDetailProdukState createState() => _BackgroundDetailProdukState();
}

class _BackgroundDetailProdukState extends State<BackgroundDetailProduk> {
  bool statusPass = false;
  bool openDesk = false;
  // BlocLoginBloc bloc = BlocLoginBloc();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 40),
                        height: size.height / 1.4,
                        alignment: Alignment.topCenter,
                        width: size.width / 7,
                        child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            })),
                    (openDesk == false)
                        ? ContainerDetailProduk(
                            child: Stack(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: new Image.network(
                                    'https://qiaminimarketjaksel.com/gambar/'+widget.images.toString(),
                                    fit: BoxFit.contain,
                                  ),
                                ),
                                (widget.diskon != '0')
                                    ? Positioned(
                                        top: -20,
                                        right: -45,
                                        child: Container(
                                            alignment: Alignment.center,
                                            width: 100,
                                            height: 20,
                                            transform: Matrix4.rotationZ(0.8),
                                            color: Colors.red,
                                            child: Text(
                                              widget.diskon.toString(),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            )),
                                      )
                                    : Container()
                              ],
                            ),
                            height: size.height / 1.4,
                            width: size.width - (size.width / 7),
                          )
                        : ContainerDetailProduk(
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(widget.deskripsi.toString(),
                                      style: TextStyle(
                                          fontFamily: 'RobotoCondensed'),
                                      textAlign: TextAlign.left),
                                ],
                              ),
                            ),
                            height: size.height / 1.4,
                            width: size.width - (size.width / 7),
                          )
                  ],
                ),
              ),
              Container(
                height: size.height - (size.height / 1.4),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        top: 10,
                        right: 10,
                        left: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(widget.namaBarang,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'RobotoCondensed')),
                          Text("Rp "+widget.harga,
                              style: TextStyle(
                                  color: (widget.diskon == "0")
                                      ? Colors.black
                                      : Colors.red,
                                  decoration: (widget.diskon == "0")
                                      ? TextDecoration.none
                                      : TextDecoration.lineThrough,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'RobotoCondensed'))
                        ],
                      ),
                    ),
                    (widget.diskon != '0')
                        ? Container(
                            padding: EdgeInsets.only(
                              top: 10,
                              right: 10,
                              left: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Harga Promo",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'RobotoCondensed')),
                                Text("Rp "+widget.hargaPromo,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'RobotoCondensed'))
                              ],
                            ),
                          )
                        : Container(),
                    Container(
                      padding: EdgeInsets.only(
                        top: 10,
                        right: 10,
                        left: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Stok",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'RobotoCondensed')),
                          Text(widget.qty.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'RobotoCondensed'))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        (openDesk == false)
                            ? SizedBox(
                                width: size.width / 2,
                                height: 74,
                                child: ElevatedButton(
                                  onPressed: () {
                                    BarangModel myBarang = BarangModel(idBarang: widget.idBarang,deskripsi: widget.deskripsi,harga: widget.harga, hargaPromo: widget.hargaPromo,namaBarang: widget.namaBarang,qty: widget.qty,image: widget.images, diskon: widget.diskon, statusCeklis: false);
                                    showModalBottomSheet(
                                        context: context,
                                        builder: (context) {
                                          return ModalBuy(data: myBarang,);
                                        });
                                  },
                                  child: Text("Pesan",
                                      style: TextStyle(
                                          color: (openDesk == true)
                                              ? Colors.black
                                              : Colors.white)),
                                  style: ButtonStyle(
                                      elevation:
                                          MaterialStateProperty.all<double>(0),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              (openDesk == true)
                                                  ? Colors.white
                                                  : background1),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  topRight:
                                                      Radius.circular(20))))),
                                ),
                              )
                            : SizedBox(
                                width: size.width / 2,
                                height: 74,
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      openDesk = false;
                                    });
                                  },
                                  child: Text(
                                    "Barang",
                                    style: TextStyle(
                                        color: (openDesk == true)
                                            ? Colors.black
                                            : Colors.white),
                                  ),
                                  style: ButtonStyle(
                                      elevation:
                                          MaterialStateProperty.all<double>(0),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              (openDesk == true)
                                                  ? Colors.white
                                                  : background1),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  topRight:
                                                      Radius.circular(20))))),
                                ),
                              ),
                        Container(
                          width: size.width / 2,
                          height: 74,
                          child: ElevatedButton(
                            onPressed: () {
                              setState(() {
                                openDesk = true;
                              });
                            },
                            child: Text(
                              "Deskripsi",
                              style: TextStyle(
                                  color: (openDesk == true)
                                      ? Colors.white
                                      : Colors.black),
                            ),
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        (openDesk == true)
                                            ? background1
                                            : Colors.white),
                                elevation: MaterialStateProperty.all<double>(0),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20))))),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }
}
