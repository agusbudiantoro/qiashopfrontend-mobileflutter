import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/models/barang/model_listBarang.dart';
import 'package:qiaShop/models/kategori/kategoriModel.dart';
import 'package:qiaShop/view_models/bloc/bloc_barang/barang_bloc.dart';
import 'package:qiaShop/view_models/bloc/bloc_kategori/kategori_bloc.dart';
import 'package:qiaShop/views/pages/detailProduk.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/data/content.dart';
import 'package:qiaShop/views/widgets/keranjang/backgroundKeranjang.dart';

class IsiFeed extends StatefulWidget {
  final int jenis;

  IsiFeed({this.jenis});
  @override
  _IsiFeedState createState() => _IsiFeedState(jenis: this.jenis);
}

class _IsiFeedState extends State<IsiFeed> {
  List<ValuesKategori> listKat;
  List<BarangModel> listIsi;
  final int jenis;
  BarangBloc blocBarang = BarangBloc();
  KategoriBloc blocKategori = KategoriBloc();
  int idKategori = 0;
  String cariBarang="";

  _IsiFeedState({this.jenis});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    blocKategori..add(GetKategoriEvent());
    if (jenis == 1) {
      blocBarang..add(EventGetBarangRekom());
    } else if (jenis == 2) {
      blocBarang..add(EventGetBarangPromo());
    } else if (jenis == 3) {
      // listIsi = dataBarangRek() + dataBarangPro();
      blocBarang..add(EventGetBarang());
    }
  }

  Flexible pilihKategori(KategoriStateSukses data) {
    return Flexible(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: data.myData.length,
        scrollDirection: Axis.horizontal,
        physics: AlwaysScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int i) {
          return Container(
            padding: EdgeInsets.only(right: 10),
            child: InputChip(
              backgroundColor: (data.myData[i].id == idKategori)
                  ? background1
                  : Colors.grey[250],
              label: Text(data.myData[i].namaKategori,
                  style: TextStyle(
                      fontFamily: 'RobotoCondensed',
                      color: (data.myData[i].id == idKategori)
                          ? Colors.white
                          : Colors.black)),
              onPressed: () {
                setState(() {
                  idKategori = data.myData[i].id;
                });
              },
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          color: background1,
          padding: EdgeInsets.only(top:size.height/15, right: 10, left: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
              child: IconButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back_ios),
              ),
            ),
              Container(
              padding: EdgeInsets.only(top:5, bottom: 10),
              // color: Colors.white,
              height: size.height/15,
              width: size.width / 1.40,
              child: TextField(
                onChanged: (val){
                  setState(() {
                    cariBarang=val.toString();
                  });
                },
                autofocus: (widget.jenis == 3)?true:false,
                style: TextStyle(
                  fontSize: 14
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(),
                  labelText: 'Cari Barang',
                ),
              ),
            ),
            Container(
              child: IconButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BackgroundKeranjang()));
                },
                icon: Icon(Icons.add_shopping_cart_sharp),
              ),
            )
            ],
          ),
        ),
        Container(
          width: size.width,
          height: size.height / 15,
          child: Row(
            children: [
              Container(
                width: size.width / 5.5,
                padding: EdgeInsets.all(10),
                child: Text("Kategori",
                    style:
                        TextStyle(fontFamily: 'RobotoCondensed', fontSize: 15)),
              ),
              Container(
                  // alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(top: 10, left: 10, bottom: 10),
                  height: 50,
                  width: size.width - (size.width / 5.5),
                  child: Row(
                    children: [
                      Container(
                            padding: EdgeInsets.only(right: 10),
                            child: InputChip(
                              backgroundColor: (idKategori == 0)?background1:Colors.grey[250],
                              label: Text("Semua",
                                  style: TextStyle(
                                      fontFamily: 'RobotoCondensed', color: (idKategori == 0)?Colors.white:Colors.black)),
                              onPressed: () {
                                setState(() {
                                  idKategori=0;
                                });
                              },
                            ),
                          ),
                      BlocBuilder<KategoriBloc, KategoriState>(
                        bloc:blocKategori,
                        builder: (context, state) {
                          if(state is KategoriStateSukses){
                            return pilihKategori(state);
                          }
                          if(state is KategoriStateLoading){
                            return Center(child: CircularProgressIndicator(),);
                          }
                          if(state is KategoriStateFailed){
                            return Container(child: Center(child: Text("Gagal Ambil Data Kategori"),),);
                          }
                          return Container();
                        },
                      )
                      ,
                    ],
                  ))
            ],
          ),
        ),
        Container(
            child: BlocBuilder<BarangBloc, BarangState>(
            bloc: blocBarang,
            builder: (context, state) {
              
              if(state is StateGetBarangSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangWaiting){
                return Center(child:CircularProgressIndicator());
              }
              if(state is StateGetBarangFailed){
                return Container(child: Center(child: Text("Gagal Mengambil Data"),),);
              }
              if(state is StateDelBarangFailed){
                return Container(child: Center(child: Text("Gagal Hapus Data"),),);
              }
              if(state is StateDelBarangWaiting){
                return Center(child:CircularProgressIndicator());
              }
              if(state is StateGetBarangRekomSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangRekomWaiting){
                return Container(child: Center(child: CircularProgressIndicator(),),);
              }
              if(state is StateGetBarangRekomFailed){
                return Container(child: Text("Gagal Mengambil Data"),);
              }
              if(state is StateGetBarangPromoSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangPromoWaiting){
                return Container(child: Center(child: CircularProgressIndicator(),),);
              }
              if(state is StateGetBarangPromoFailed){
                return Container(child: Text("Gagal Mengambil Data"),);
              }
              return Container();
            },
          ),
          ),
        
      ],
    );
  }

  Expanded listBodyFeed(Size size,List<ValuesListBarang> data) {
    List<ValuesListBarang> listData = List<ValuesListBarang>();
      if(cariBarang.toString().length >0){
        if(idKategori == 0){
          listData = data.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase())).toList();
        } else if(idKategori != 0){
          listData = data.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase()) && i.kategori == idKategori).toList();
        }
      } else if(cariBarang.toString().length == 0){
        if(idKategori == 0){
          listData = data;
        } else if(idKategori != 0){
          listData = data.where((i) => i.kategori == idKategori).toList();
        }
      }
    return Expanded(
          child: Padding(
        padding: EdgeInsets.all(5),
        child: GridView.builder(
          scrollDirection: Axis.vertical,
          itemCount: listData.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailProduk(
                              idBarang: listData[index].idBarang,
                              images: listData[index].gambar.toString(),
                              hargaPromo: listData[index].hargaPromo.toString(),
                              namaBarang: listData[index].namaBarang.toString(),
                              harga: listData[index].harga.toString(),
                              deskripsi: listData[index].deskripsi.toString(),
                              qty: listData[index].qty,
                              diskon: listData[index].promo.toString(),
                            )));
              },
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Container(
                      padding: EdgeInsets.all(5),
                      color: Colors.white,
                      child: Column(
                        children: [
                          Container(
                            height: 100,
                            width: 200,
                            // color: Colors.white,
                            child: Stack(
                              children: [
                                Container(
                                    color: Colors.white,
                                    width: size.width,
                                    height: size.height / 6,
                                    child: new Image.network(
                                      'https://qiaminimarketjaksel.com/gambar/'+listData[index].gambar.toString(),
                                      fit: BoxFit.contain,
                                    )),
                                (listData[index].promo != 0)?Positioned(
                                  top: -20,
                                  right: -45,
                                  child: Container(
                                      alignment: Alignment.center,
                                      width: 100,
                                      height: 20,
                                      transform: Matrix4.rotationZ(0.8),
                                      color: Colors.red,
                                      child: Text(
                                        listData[index].promo.toString()+"%",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                                ):Container(),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text(listData[index].namaBarang.toString()),
                          ),
                          (listData[index].promo != 0)
                              ? Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                      "Rp "+listData[index].hargaPromo.toString()),
                                )
                              : Container(),
                          (listData[index].promo == 0)
                              ? Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text("Rp "+listData[index].harga.toString()),
                                )
                              : Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                    "Rp "+listData[index].harga.toString(),
                                    style: TextStyle(
                                      color: Colors.red,
                                        fontSize: 12,
                                        decoration:
                                            TextDecoration.lineThrough),
                                  ),
                                )
                        ],
                      ))),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
          ),
        ),
      ));
  }
}
