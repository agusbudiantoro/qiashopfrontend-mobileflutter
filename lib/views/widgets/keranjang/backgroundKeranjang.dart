import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/models/keranjang/barang.dart';
import 'package:qiaShop/view_models/bloc/barangKeranjang_bloc/barangkeranjang_bloc.dart';
import 'package:qiaShop/views/utils/content/images.dart';
import 'package:qiaShop/views/utils/data/content.dart';
import 'package:qiaShop/views/widgets/pembayaran/bayar.dart';

import 'del.dart';

class BackgroundKeranjang extends StatefulWidget {
  final int jenis;
  BackgroundKeranjang({this.jenis});
  @override
  _BackgroundKeranjangState createState() => _BackgroundKeranjangState();
}

class _BackgroundKeranjangState extends State<BackgroundKeranjang> {
  bool isChecked = false;
  List<BarangModel> listKer;
  BarangkeranjangBloc blocKeranjang = BarangkeranjangBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listKer = dataBarangKeranjang();
    getBarangKeranjangWithBloc();
  }

  void getBarangKeranjangWithBloc(){
    blocKeranjang..add(EventGetBarangKeranjangByIdKonsumen());
  }

  Future<void> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return blocKeranjang..add(EventGetBarangKeranjangByIdKonsumen());
  }

  void hapusBarang(id){
    blocKeranjang..add(EventDelBarangKeranjang(id:id));
  }

  myDialog(id,BuildContext context){
    showDialog(
        context: context,
        builder: (BuildContext context) => CustomDialogKeranjang(clickCallback: (){
          hapusBarang(id);
        },),
      );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.black;
    }

    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Center(
              child: Text(
            "Keranjang",
            style: TextStyle(color: Colors.white),
          )),
        ),
        body: BlocListener<BarangkeranjangBloc, BarangkeranjangState>(
          bloc: blocKeranjang,
          listener: (context, state) {
            if(state is StateDelBarangKeranjangSukses){
              return getBarangKeranjangWithBloc();
            }
          },
          child: Container(
            child: BlocBuilder<BarangkeranjangBloc, BarangkeranjangState>(
              bloc: blocKeranjang,
              builder: (context, state) {
                if (state is StateGetBarangKeranjangByIdKonsumenSukses) {
                  if (state.data.length != 0) {
                    return listView(size, getColor, state.data);
                  } else {
                    return Container(
                      child: Center(
                        child: Text("data kosong"),
                      ),
                    );
                  }
                }
                if (state is StateGetBarangKeranjangByIdKonsumenFailed) {
                  return Container(
                    child: Center(
                      child: Text("Gagal Memuat Data"),
                    ),
                  );
                }
                if (state is StateGetBarangKeranjangByIdKonsumenWaiting) {
                  return Container(
                      child: Center(child: CircularProgressIndicator()));
                }
                if (state is StateDelBarangKeranjangFailed) {
                  return Container(
                    child: Center(
                      child: Text("Gagal Hapus Data"),
                    ),
                  );
                }
                if (state is StateDelBarangKeranjangWaiting) {
                  return Center(child: CircularProgressIndicator());
                }
                return Container(
                  child: Center(
                    child: Text("data kosong"),
                  ),
                );
              },
            ),
          ),
        ));
  }

  RefreshIndicator listView(Size size, Color getColor(Set<MaterialState> states), List<ValuesListBarangKeranjang> data) {
    return RefreshIndicator(
      onRefresh: reffreshIn,
      child: ListView.builder(
          itemCount: data.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int i) {
            return Card(
              color: Colors.white,
              // height: size.height / 5,
              child: Column(
                children: [
                  Container(
                    padding:EdgeInsets.all(5),
                    height: (size.height / 3) / 1.7,
                    child: Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // Container(
                          //   alignment: Alignment.topLeft,
                          //   child: Checkbox(
                          //     checkColor: Colors.white,
                          //     fillColor:
                          //         MaterialStateProperty.resolveWith(getColor),
                          //     value: listKer[i].statusCeklis,
                          //     onChanged: (value) {
                          //       setState(() {
                          //         print(value);
                          //         print(!value);
                          //         listKer[i].statusCeklis = value;
                          //       });
                          //     },
                          //   ),
                          // ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            alignment: Alignment.topLeft,
                            width: size.width / 1.8,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.topLeft,
                                  width: size.width / 1.8,
                                  child: new Text(
                                    data[i].namaBarang.toString(),
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                                Spacer(),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 5),
                                  child: Row(
                                    children: [
                                      (data[i].promo != 0)
                                          ? Text(
                                              "Rp "+data[i].hargaPromo.toString(),
                                              style: TextStyle(
                                                  color: Colors.black),
                                              textAlign: TextAlign.left)
                                          : Text("-",
                                              style: TextStyle(
                                                  color: Colors.black),
                                              textAlign: TextAlign.left),
                                      Spacer(),
                                      (data[i].promo == 0)
                                          ? Text("Rp "+data[i].hargaBarang.toString(),
                                              style: TextStyle(
                                                  color: Colors.black))
                                          : Text("Rp "+data[i].hargaBarang.toString(),
                                              style: TextStyle(
                                                  color: Colors.red,
                                                  decoration: TextDecoration
                                                      .lineThrough))
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 5),
                                  child: Row(
                                    children: [
                                      (data[i].promo != 0)
                                          ? Text(
                                              "Diskon " +
                                                  data[i].promo.toString()+"%",
                                              style: TextStyle(
                                                  color: Colors.black),
                                              textAlign: TextAlign.left)
                                          : Text("Tidak ada diskon",
                                              style: TextStyle(
                                                  color: Colors.black),
                                              textAlign: TextAlign.left),
                                      Spacer(),
                                      ElevatedButton(
                                          onPressed: () {
                                            BarangModel myData = BarangModel(idKeranjang: data[i].idKeranjang, stok: data[i].stok,image: data[i].gambar,totalHarga: data[i].totalHarga, idBarang: data[i].idBarang, deskripsi: data[i].deskripsi, harga: data[i].hargaBarang.toString(), hargaPromo: data[i].hargaPromo.toString(),namaBarang: data[i].namaBarang, qty: data[i].qty, diskon: data[i].promo.toString(),kategori: data[i].kategori.toString()); 
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=>Pagebayar(dataBarang: myData,)));
                                          },
                                          child: Text("Bayar",
                                              style: TextStyle(
                                                  color: Colors.white)))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.topCenter,
                              width: (size.width - size.width / 1.8) / 2 - 10,
                              child: Column(
                                children: [
                                  Container(
                                    color: Colors.white,
                                    height: 50,
                                    width: 50,
                                    child: new Image.network(
                                      'https://qiaminimarketjaksel.com/gambar/'+data[i].gambar.toString(),
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                      alignment: Alignment.bottomCenter,
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                          size: 30,
                                        ),
                                        onPressed: () {
                                          myDialog(data[i].idKeranjang, context);
                                        },
                                      ))
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
    );
  }
}
