import 'package:flutter/material.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_buttonNavigationBar.dart';
import 'package:qiaShop/views/widgets/home/background.dart';
import 'package:qiaShop/views/widgets/keranjang/backgroundKeranjang.dart';
import 'package:qiaShop/views/widgets/pembayaran/historiPembayaran.dart';
import 'package:qiaShop/views/widgets/profile/profile.dart';

class BackgroundMenu extends StatefulWidget {
  final page;
  BackgroundMenu({this.page});
  @override
  _BackgroundMenuState createState() => _BackgroundMenuState(page:this.page);
}

class _BackgroundMenuState extends State<BackgroundMenu> {
  bool statusPass = false;
  _BackgroundMenuState({this.page});
  int page;
  // BlocLoginBloc bloc = BlocLoginBloc();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: (page == 0)?BackgroundHome():(page == 1)?BackgroundKeranjang():(page == 2)?BackgroundHistori():(page == 3)?BackgroundProfil():Container(),
      bottomNavigationBar: BottomBarHome(
        page:page,
        selectPage: (int val){
        setState(() {
          page=val;
        });
      },),
    );
  }
}
