import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:qiaShop/view_models/bloc/history_pembayaran_bloc/historypembayaran_bloc.dart';
import 'package:qiaShop/views/utils/content/images.dart';
import 'package:qiaShop/views/utils/data/content.dart';
import 'package:qiaShop/views/widgets/pembayaran/bayar.dart';
import 'package:qiaShop/views/widgets/pembayaran/detailPembelian.dart';

class BackgroundHistori extends StatefulWidget {
  final int jenis;
  BackgroundHistori({this.jenis});
  @override
  _BackgroundHistoriState createState() => _BackgroundHistoriState();
}

class _BackgroundHistoriState extends State<BackgroundHistori> {
  bool isChecked = false;
  List<BarangModel> listKer;
  HistorypembayaranBloc blocHistory = HistorypembayaranBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listKer = dataBarangKeranjang();
    blocHistory..add(EventGetHistoryPembayaran());
  }

  Future<void> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return blocHistory..add(EventGetHistoryPembayaran());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Center(
              child: Text(
            "Pembayaran",
            style: TextStyle(color: Colors.white),
          )),
        ),
        body: BlocBuilder<HistorypembayaranBloc, HistorypembayaranState>(
          bloc: blocHistory,
          builder: (context, state) {
            if(state is StateHistoryPembayaranSukses){
              if (state.data != null) {
                    return listData(size, state.data);
                  } else {
                    return Container(
                      child: Center(
                        child: Text("data kosong"),
                      ),
                    );
                  }
            }
            if(state is StateHistoryPembayaranWaiting){
              return Container(child:Center(child:CircularProgressIndicator()));
            }
            if(state is StateHistoryPembayaranFailed){
              return Container(child:Center(child: CircularProgressIndicator(),));
            }
            return Container(child: Center(child: Text("Initial State"),),);
          },
        )
        );
  }

  RefreshIndicator listData(Size size, List<ValueGetPemesanan> dataHistory) {
    return RefreshIndicator(
        onRefresh: reffreshIn,
        child: ListView.builder(
          itemCount: dataHistory.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int i) {
            return GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>PageDetailPembelian(dataPesanan: dataHistory[i])));
              },
                  child: Container(
                  // padding: EdgeInsets.all(5),
                  height: size.height / 7,
                  child: Card(
                    child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left:5,right:5),
                                color: Colors.white,
                                height: 75,
                                width: 70,
                                child: new Image.network(
                                  'https://qiaminimarketjaksel.com/gambar/'+dataHistory[i].image.toString(),
                                  fit: BoxFit.contain,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 25, left: 10,right:10),
                                alignment: Alignment.topLeft,
                                width: size.width / 1.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topLeft,
                                      width: size.width / 1.8,
                                      child: new Text(
                                        dataHistory[i].namaBarang.toString(),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.topLeft,
                                      width: size.width / 1.8,
                                      child: new Text(
                                        (dataHistory[i].statusPengiriman == 1)?"Barang Sedang Disiapkan":(dataHistory[i].statusPengiriman == 2)?"Barang Sedang Dikirim":(dataHistory[i].statusPengiriman == 3)?"Barang Sudah diKlaim":"",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 20, left: 10,right:10),
                                alignment: Alignment.center,
                                width: size.width / 4.5,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topLeft,
                                      width: size.width / 1.8,
                                      child: new Text(
                                        "Rp "+dataHistory[i].totalHarga.toString(),
                                        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top:10),
                                      alignment: Alignment.topLeft,
                                      width: size.width / 1.8,
                                      child: new Text(
                                        dataHistory[i].status,
                                        style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 12),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                    ],
                  ),
                  )
                ),
            );
          },
        ),
      );
  }
}
