import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/models/metodePembayaran/metodePembayaran.dart';
import 'package:qiaShop/models/pesanan/modelRiwayatPesanan.dart';
import 'package:qiaShop/view_models/bloc/pesanan_bloc/pesanan_bloc.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/content/images.dart';
import 'package:qiaShop/views/utils/custom_widgets/keranjang/detailKeranjang/alamatPengiriman.dart';
import 'package:qiaShop/views/utils/custom_widgets/keranjang/detailKeranjang/daftarBelanja.dart';
import 'package:qiaShop/views/utils/custom_widgets/keranjang/detailKeranjang/metodePembayaran.dart';
import 'package:qiaShop/views/utils/custom_widgets/keranjang/detailKeranjang/metodePengiriman.dart';
import 'package:qiaShop/views/utils/custom_widgets/keranjang/detailKeranjang/rincianHarga.dart';
import 'package:qiaShop/views/widgets/menu/backgroundMenu.dart';

class Pagebayar extends StatefulWidget {
  final BarangModel dataBarang;
  Pagebayar({this.dataBarang, });
  @override
  _PagebayarState createState() => _PagebayarState();
}

class _PagebayarState extends State<Pagebayar> {
  ValuesMetodePembayaranModel dataPembayaran = ValuesMetodePembayaranModel();
  PesananBloc blocPesanan = PesananBloc();
  int qty;
  int total_hargaBarang;
  int idMetodePembayaran;
  String alamatPengiriman;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    qty = widget.dataBarang.qty;
    total_hargaBarang = widget.dataBarang.totalHarga;
  }
  doSomethingMetodePembayaran(ValuesMetodePembayaranModel data) { 
    setState(() {
      idMetodePembayaran=(data != null)?data.idMetodePembayaran:0;
    });
  }
  doSomethingAlamatPengiriman(String alamat) { 
    setState(() {
      alamatPengiriman=alamat;
    });
    print("pagebayar");
    print(alamatPengiriman);
  }

  void _clickCallBackPlus() {
    setState(() {
      if (qty < widget.dataBarang.stok && qty >= 1) {
        qty += 1;
        if(int.parse(widget.dataBarang.diskon) != 0){
          total_hargaBarang = int.parse(widget.dataBarang.hargaPromo) * qty;
        } else {
          total_hargaBarang = int.parse(widget.dataBarang.harga) * qty;
        }
      }
    });
  }

  void _clickCallBackMin() {
    setState(() {
      if (qty <= widget.dataBarang.stok && qty > 1) {
        qty -= 1;
        if(int.parse(widget.dataBarang.diskon) != 0){
          total_hargaBarang = int.parse(widget.dataBarang.hargaPromo) * qty;
        } else {
          total_hargaBarang = int.parse(widget.dataBarang.harga) * qty;
        }
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Pembayaran",
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.grey[350],
        child: ListView(
          children: [
            SizedBox(
              height: 10,
            ),
            WidgetAlamat(width: size.width,height: size.height,callback: doSomethingAlamatPengiriman),
            SizedBox(
              height: 10,
            ),
            WidgetMetodePembayaran(height: size.height,width: size.width,callback: doSomethingMetodePembayaran,),
            SizedBox(
              height: 5,
            ),
            WidgetDaftarBelanja(height: size.height,width: size.width,dataBarang: widget.dataBarang,clickCallbackPlus: ()=>_clickCallBackPlus(),clickCallbackMin: ()=>_clickCallBackMin()),
            WidgetPengiriman(height: size.height,width: size.width),
            SizedBox(
              height: 10,
            ),
            WidgetRincianHarga(height: size.height,width: size.width, totalHargaBarang: total_hargaBarang,)
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.centerLeft,
        height: size.height/9,
        color: Colors.white,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Pembayaran", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                Text("Rp "+(total_hargaBarang+10000).toString(), style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold))
              ],
            ),
            BlocListener<PesananBloc, PesananState>(
              bloc: blocPesanan,
              listener: (context, state) {
                // TODO: implement listener
                if (state is StatePostPesananSukses) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Transaksi Berhasil"),
                      duration: Duration(milliseconds: 500),
                      backgroundColor: background1,
                    )
                  );
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>BackgroundMenu(page: 2)));
                }
                if (state is StatePostPesananFailed) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Transaksi Gagal, "+state.errorMessage.toString()),
                      duration: Duration(milliseconds: 500),
                      backgroundColor: Colors.red,
                    )
                  );
                }
              },
              child: Container(
                child: BlocBuilder<PesananBloc, PesananState>(
                bloc: blocPesanan,
                builder: (context, state) {
                  if(state is StatePostPesananSukses){
                    return buttonBayar(size);
                  }
                  if(state is StatePostPesananWaiting){
                    return Center(child: CircularProgressIndicator(backgroundColor: Colors.red,));
                  }
                  if(state is StatePostPesananFailed){
                    return buttonBayar(size);
                  }
                  return buttonBayar(size);
                },
              ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container buttonBayar(Size size) {
    return Container(
            width: size.width,
            child: ElevatedButton(
              onPressed: (){
                ModelRiwayatPesanan dataPesanan = ModelRiwayatPesanan(idKeranjang: widget.dataBarang.idKeranjang ,metodePembayaran: idMetodePembayaran, alamat: alamatPengiriman, idBarang: widget.dataBarang.idBarang,namaBarang: widget.dataBarang.namaBarang,qty: qty, harga: (int.parse(widget.dataBarang.diskon) == 0)?int.parse(widget.dataBarang.harga):int.parse(widget.dataBarang.hargaPromo), totalHarga: total_hargaBarang+10000); 
                blocPesanan..add(EventPostPesanan(data: dataPesanan));
              },
              child: Text("Bayar dengan Virtual Account", style: TextStyle(color: Colors.white),),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.red)
              ),
            )

          );
  }
}
