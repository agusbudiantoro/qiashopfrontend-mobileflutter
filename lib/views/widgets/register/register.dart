import 'package:flutter/material.dart';
import 'package:qiaShop/models/login/model_form_login.dart';
import 'package:qiaShop/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:qiaShop/views/admin/pages/menu.dart';
import 'package:qiaShop/views/pages/menu.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/custom_widgets/login/custom_button.dart';
import 'package:qiaShop/views/utils/custom_widgets/login/custom_container.dart';
import 'package:qiaShop/views/utils/custom_widgets/login/custom_sphare.dart';
import 'package:qiaShop/views/utils/custom_widgets/login/custom_textField.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BackgroundRegister extends StatefulWidget {
  @override
  _BackgroundRegisterState createState() => _BackgroundRegisterState();
}

class _BackgroundRegisterState extends State<BackgroundRegister> {
  bool statusPass = true;
  BlocLoginBloc bloc = BlocLoginBloc();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _noHp = TextEditingController();
  final TextEditingController _alamat = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
          child: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [circlePurpleDark, background1],
                begin: Alignment.topRight,
                end: Alignment.bottomRight)),
        child: Stack(
          children: [
            Positioned(
                left: -250,
                top: 20,
                right: size.height * 0.1,
                child: SphareLogin(
                  height: 200,
                  width: 200,
                )),
            Positioned(
                right: -250,
                bottom: -20,
                left: size.height * 0.1,
                child: SphareLogin(
                  height: 200,
                  width: 200,
                )),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // Container(
                //   alignment: Alignment.centerLeft,
                //   // margin: EdgeInsets.symmetric(horizontal: size.width * 0.075),
                //   child: ContainerLogin(
                //     width: 60.0,
                //     height: 60.0,
                //     borderRadius: 10.0,
                //     child: Icon(
                //       Icons.info_outline,
                //       color: Colors.white,
                //     ),
                //   ),
                // ),
                Container(
                  alignment: Alignment.center,
                  child: ContainerLogin(
                    width: size.width * 0.8,
                    height: size.height * 0.65,
                    borderRadius: 10.0,
                    child: Column(
                      children: [
                        Spacer(),
                        Text(
                          "Daftar Qia",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30.0,
                              fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                        ),
                        Spacer(),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: false,
                          isiField: _username,
                          hintText: "Username",
                          prefixIcon: Icons.account_circle_rounded,
                        ),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: false,
                          isiField: _email,
                          hintText: "Email",
                          prefixIcon: Icons.email,
                        ),
                        TextFieldLogin(
                          typePass: true,
                          typeInput: false,
                            isiField: _passWord,
                            hintText: "Password",
                            prefixIcon: Icons.vpn_key_rounded,
                            isObsercure: statusPass,
                            suffixIcon: Icons.remove_red_eye,
                            clickCallback: () => _clickCallBack()),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: true,
                          isiField: _noHp,
                          hintText: "Nomor Hp",
                          prefixIcon: Icons.phone_android,
                        ),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: false,
                          isiField: _alamat,
                          hintText: "Alamat",
                          prefixIcon: Icons.home,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          width: size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              BlocListener<BlocLoginBloc, BlocLoginState>(
                              bloc: bloc,
                              listener: (context, state) {
                                if(state is BlocStateRegistSukses){
                                  Navigator.pop(context);
                                  // if(state.myData.role == 1){
                                  //   Navigator.push(
                                  //     context, MaterialPageRoute(builder: (context) => BackgroundMenuAdmin(page: 0,)));
                                  // } else {
                                  //   Navigator.push(
                                  //     context, MaterialPageRoute(builder: (context) => Menu()));
                                  // }
                                }
                              },
                              child: Container(
                                child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
                                  bloc: bloc,
                                  builder: (context, state) {
                                    if (state is BlocStateRegistSukses) {
                                      return buildButtonLogin();
                                    }
                                    if (state is BlocStateRegistLoading) {
                                      return Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    if (state is BlocaStateFailed) {
                                      return buildButtonLogin();
                                    }
                                    if (state is BlocLoginInitial) {
                                      return buildButtonLogin();
                                    }
                                    return Container();
                                  },
                                ),
                              ),
                            ),
                            ButtonLogin(
                              onPress: (){
                                Navigator.pop(context);
                              },
                              buttonName: "Kembali",
                              paddingH: 35.0,
                            )
                            ],
                          ),
                        ),
                        Spacer()
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      )),
    );
  }

  ButtonLogin buildButtonLogin() {
    return ButtonLogin(
      onPress: (){
        MyDataFormLogin isi = MyDataFormLogin(email: _email.text.toString(), password: _passWord.text.toString(), username: _username.text.toString(), noHp: _noHp.text.toString(), alamat: _alamat.text.toString());
        bloc..add(BlocRegister(data: isi));
      },
      buttonName: "Daftar",
      paddingH: 35.0,
    );
  }
}
