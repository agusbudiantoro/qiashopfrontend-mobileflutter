import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/view_models/bloc/bloc_barang/barang_bloc.dart';
import 'package:qiaShop/views/pages/detailProduk.dart';
import 'package:qiaShop/views/pages/feeds.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/content/images.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_container.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_container_child.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_search.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_sphare.dart';
import 'package:qiaShop/views/utils/data/content.dart';

class BackgroundHome extends StatefulWidget {
  @override
  _BackgroundHomeState createState() => _BackgroundHomeState();
}

class _BackgroundHomeState extends State<BackgroundHome> {
  bool statusPass = false;
  List<BarangModel> listRek;
  List<BarangModel> listPro;
  BarangBloc blocBarang = BarangBloc();
  BarangBloc blocBarangPromo = BarangBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listRek = dataBarangRek();
    listPro = dataBarangPro();
    blocBarang..add(EventGetBarangRekom());
    blocBarangPromo..add(EventGetBarangPromo());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [circlePurpleDark, background1],
                begin: Alignment.topRight,
                end: Alignment.bottomRight)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: size.height / 3,
                padding: EdgeInsets.only(bottom: 60),
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      child: SphareHome(
                        height: size.height / 5,
                        width: size.width,
                      ),
                    ),
                    Positioned(
                      bottom: 15,
                      child: Container(
                        width: size.width,
                        alignment: Alignment.center,
                        // padding: EdgeInsets.only(left: 38, right: 20),
                        child: SearchHome(
                          height: size.height / 16,
                          width: (size.width * 5) / 6,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30),
                child: ContainerHome(
                    height: size.height / 2.5,
                    width: size.width,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Title(
                                color: Colors.white,
                                child: Text(
                                  "Rekomendasi",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ),
                              Spacer(),
                              TextButton(
                                child: Text(
                                  "Lihat",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FeedPage(
                                                jenis: 1,
                                              )));
                                },
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.teal),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                            side: BorderSide(
                                                color: Colors.green[200])))),
                              )
                            ],
                          ),
                          // Spacer(),
                          BlocBuilder<BarangBloc, BarangState>(
                            bloc: blocBarang,
                            builder: (context, state) {
                              if(state is StateGetBarangRekomSukses){
                                return listRekom(size, state);
                              }
                              if(state is StateGetBarangRekomWaiting){
                                return Container(child: Center(child: CircularProgressIndicator(),),);
                              }
                              if(state is StateGetBarangRekomFailed){
                                return Container(child: Text("Gagal Mengambil Data"),);
                              }
                              return Container(child:Text("Gagal Ambil Data"));
                            },
                          )
                        ],
                      ),
                    )),
              ),
              SizedBox(height: 50),
              Padding(
                padding: EdgeInsets.only(left: 30),
                child: ContainerHome(
                    height: size.height / 2.8,
                    width: size.width,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5,
                              ),
                              Title(
                                  color: Colors.white,
                                  child: Text(
                                    "Promosi",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  )),
                              Spacer(),
                              TextButton(
                                child: Text(
                                  "Lihat",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FeedPage(
                                                jenis: 2,
                                              )));
                                },
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.teal),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                            side: BorderSide(
                                                color: Colors.green[200])))),
                              )
                            ],
                          ),
                          BlocBuilder<BarangBloc, BarangState>(
                            bloc: blocBarangPromo,
                            builder: (context, state) {
                              if(state is StateGetBarangPromoSukses){
                                return listPromo(size, state);
                              }
                              if(state is StateGetBarangPromoWaiting){
                                return Container(child: Center(child: CircularProgressIndicator(),),);
                              }
                              if(state is StateGetBarangPromoFailed){
                                return Container(child: Text("Gagal Mengambil Data"),);
                              }
                              return Container(child:Text("Gagal Ambil Data"));
                            },
                          )
                        ],
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Expanded listPromo(Size size, StateGetBarangPromoSukses data) {
    return Expanded(
                            child: ListView.builder(
                                itemCount: 3,
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                physics: AlwaysScrollableScrollPhysics(),
                                itemBuilder: (BuildContext context, int i) {
                                  return GestureDetector(
                                    onTap: () {
                                      // Navigator.pop(context, true);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailProduk(
                                                    idBarang: data.listData[i].idBarang,
                                                    images: data.listData[i].gambar,
                                                    namaBarang:
                                                        data.listData[i].namaBarang,
                                                    harga: data.listData[i].harga.toString(),
                                                    deskripsi:
                                                        data.listData[i].deskripsi,
                                                    qty: data.listData[i].qty,
                                                    diskon: data.listData[i].promo.toString(),
                                                    hargaPromo:
                                                        data.listData[i].hargaPromo.toString(),
                                                  )));
                                    },
                                    child: ContainerHomeChild(
                                      child: Column(
                                        children: [
                                          Container(
                                            color: Colors.white,
                                            width: size.width,
                                            height: size.height / 6,
                                            child: Stack(
                                              children: [
                                                Container(
                                                    color: Colors.white,
                                                    width: size.width,
                                                    height: size.height / 6,
                                                    child: new Image.network(
                                                      'https://qiaminimarketjaksel.com/gambar/'+data.listData[i].gambar.toString(),
                                                      fit: BoxFit.contain,
                                                    )),
                                                Positioned(
                                                  top: -20,
                                                  right: -45,
                                                  child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      width: 100,
                                                      height: 20,
                                                      transform:
                                                          Matrix4.rotationZ(
                                                              0.8),
                                                      color: Colors.red,
                                                      child: Text(
                                                        data.listData[i]
                                                            .promo.toString()+"%",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Column(
                                              children: [
                                                Text(
                                                  data.listData[i].namaBarang,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Text("Rp "+data.listData[i].harga.toString(),
                                                    style: TextStyle(
                                                        color: Colors.red,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                        fontSize: 12)),
                                                Text("Rp "+data.listData[i].hargaPromo.toString(),
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      height: size.height / 4,
                                      width: size.width / 1.5,
                                    ),
                                  );
                                }));
  }

  Expanded listRekom(Size size, StateGetBarangRekomSukses data) {
    return Expanded(
                            child: ListView.builder(
                                itemCount: 3,
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                physics: AlwaysScrollableScrollPhysics(),
                                itemBuilder:
                                    (BuildContext context, int index) {
                                  return Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          // Navigator.pop(context, true);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailProduk(
                                                        idBarang: data.listData[index].idBarang,
                                                        images: data.listData[index]
                                                            .gambar,
                                                        namaBarang:
                                                            data.listData[index]
                                                                .namaBarang,
                                                        harga: data.listData[index]
                                                            .harga.toString(),
                                                        deskripsi:
                                                            data.listData[index]
                                                                .deskripsi,
                                                        qty: data.listData[index]
                                                            .qty,
                                                            hargaPromo: data.listData[index].hargaPromo.toString(),
                                                            diskon: data.listData[index].promo.toString(),
                                                      )));
                                        },
                                        child: ContainerHomeChild(
                                          child: Column(
                                            children: [
                                              Container(
                                                color: Colors.white,
                                                width: size.width,
                                                height: size.height / 5.5,
                                                child: new Image.network(
                                      'https://qiaminimarketjaksel.com/gambar/'+data.listData[index].gambar.toString(),
                                                  fit: BoxFit.contain,
                                                ),
                                              ),
                                              Container(
                                                padding:
                                                    EdgeInsets.only(top: 10),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      data.listData[index]
                                                          .namaBarang,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight
                                                                  .bold),
                                                    ),
                                                    Text("Rp "+data.listData[index].harga.toString(),
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white))
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          height: size.height / 3.9,
                                          width: size.width / 2.7,
                                        ),
                                      ),
                                    ],
                                  );
                                }));
  }
}
