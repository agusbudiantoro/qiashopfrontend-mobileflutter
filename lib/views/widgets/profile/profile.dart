import 'package:flutter/material.dart';
import 'package:qiaShop/models/barang/model_barang.dart';
import 'package:qiaShop/views/pages/detailProduk.dart';
import 'package:qiaShop/views/pages/feeds.dart';
import 'package:qiaShop/views/pages/login.dart';
import 'package:qiaShop/views/utils/colors.dart';
import 'package:qiaShop/views/utils/content/images.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_container_child.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_search.dart';
import 'package:qiaShop/views/utils/custom_widgets/home/custom_sphare.dart';
import 'package:qiaShop/views/utils/custom_widgets/profil/custom_container.dart';
import 'package:qiaShop/views/utils/custom_widgets/profil/header_profil.dart';
import 'package:qiaShop/views/utils/data/content.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/avd.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qiaShop/views/widgets/login/background.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BackgroundProfil extends StatefulWidget {
  @override
  _BackgroundProfilState createState() => _BackgroundProfilState();
}

class _BackgroundProfilState extends State<BackgroundProfil> {
  bool statusPass = false;
  List<BarangModel> listRek;
  List<BarangModel> listPro;
  String username;
  String email;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listRek = dataBarangRek();
    listPro = dataBarangPro();
    getAkun();
  }

  void getAkun()async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username= preferences.getString("username");
      email = preferences.getString("myEmail");
    });
  }

  void hapusPrefer()async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("username");
      await preferences.remove("token");
      await preferences.remove("myId");
      await preferences.remove("myRole");
      await preferences.remove('myFoto');
      await preferences.remove("noHp");
      await preferences.remove("myAlamat");
      await preferences.remove("myEmail");
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [circlePurpleDark, background1],
                begin: Alignment.topRight,
                end: Alignment.bottomRight)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: size.height / 3,
                padding: EdgeInsets.only(bottom: 10),
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      child: SphareProfil(
                        username: username.toString(),
                        email: email.toString(),
                        height: size.height / 3.5,
                        width: size.width,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                          width: size.width,
                          alignment: Alignment.center,
                          // padding: EdgeInsets.only(left: 38, right: 20),
                          // child: SearchHome(
                          //   height: size.height / 16,
                          //   width: (size.width * 5) / 6,
                          // ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: SvgPicture.asset(
                              avatar,
                              width: 90,
                            ),
                          )),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0),
                child: ContainerProfil(
                    height: size.height,
                    width: size.width,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 50,
                          ),
                          Flexible(
                            child: Row(
                              children: [
                                // GestureDetector(
                                //   onTap: () {
                                //     // Navigator.pop(context, true);
                                //   },
                                //   child: ContainerHomeChild(
                                //     child: Column(
                                //       children: [
                                //         Container(
                                //           color: Colors.transparent,
                                //           width: size.width,
                                //           height: size.height / 10,
                                //           child: Icon(Icons.vpn_key, color: Colors.white, size: 80,),
                                //         ),
                                //         Container(
                                //           padding: EdgeInsets.only(top: 10),
                                //           child: Column(
                                //             children: [
                                //               Text(
                                //                 "Ganti Password",
                                //                 style: TextStyle(
                                //                     color: Colors.white,
                                //                     fontWeight:
                                //                         FontWeight.bold),
                                //               )
                                //             ],
                                //           ),
                                //         ),
                                //       ],
                                //     ),
                                //     height: size.height / 5.9,
                                //     width: size.width / 2.7,
                                //   ),
                                // ),
                                // Spacer(),
                                GestureDetector(
                                  onTap: () {
                                    hapusPrefer();
                                    return Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>Login()), (Route<dynamic> route) => false);
                                  },
                                  child: ContainerHomeChild(
                                    child: Column(
                                      children: [
                                        Container(
                                          color: Colors.transparent,
                                          width: size.width,
                                          height: size.height / 10,
                                          child: Icon(Icons.exit_to_app_outlined, color: Colors.white,size: 80,),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 10),
                                          child: Column(
                                            children: [
                                              Text(
                                                "Keluar",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    height: size.height / 5.9,
                                    width: size.width / 2.7,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
