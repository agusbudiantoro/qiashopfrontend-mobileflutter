
// content rekomendasi
const rekBear = "assets/images/sembako/bearBrand.png";
const rekDancow = "assets/images/sembako/dancow.png";
const rekFilma = "assets/images/sembako/filma.jpg";
const rekGulaku = "assets/images/sembako/gulaku.jpeg";
const rekSosro = "assets/images/sembako/sosro.png";

// content promosi
const proIndomie = "assets/images/sembako/indomie.jpg";
const proMilo = "assets/images/sembako/milo.jpeg";
const proRegal = "assets/images/sembako/regalBiskuit.jpeg";
const proSariwangi = "assets/images/sembako/sariwangi.jpg";
const proSunlight = "assets/images/sembako/sunlight.png";

// logo bank
const logoBankBri = "assets/images/logo/logo-bri.png";

// logo expedisi
const logoGrab = "assets/images/logo/logo-grab.png";

//avatar
const avatar = "assets/images/avatar/avatar.svg";