import 'dart:ui';

import 'package:flutter/material.dart';

class SphareProfil extends StatefulWidget {
  final double width;
  final double height;
  final Widget child;
  final String username;
  final String email;

  SphareProfil({this.height, this.width, this.child, this.email, this.username});
  @override
  _SphareProfilState createState() => _SphareProfilState();
}

class _SphareProfilState extends State<SphareProfil> {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    var size = MediaQuery.of(context).size;
    return Container(
      // margin: EdgeInsets.only(left: 10,top: 10/2,bottom: 10*2.5),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(80.0),
            bottomRight: Radius.circular(80.0)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                    alignment: Alignment.bottomCenter,
                    width: size.width,
                    height: size.height / 6,
                    color: Colors.transparent,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.st,
                      children: [
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(widget.username.toString(), style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                        SizedBox(height:10),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(widget.email.toString(), style: TextStyle(color: Colors.white, fontSize: 10, fontWeight: FontWeight.normal),)
                            ],
                          ),
                        )
                      ],
                    )),
              ],
            ),
            height: widget.height,
            width: widget.width,
            decoration: BoxDecoration(
                gradient: RadialGradient(radius: 50, colors: [
              Colors.white.withOpacity(0.20),
              Colors.white.withOpacity(0.1)
            ])),
          ),
        ),
      ),
    );
  }
}
