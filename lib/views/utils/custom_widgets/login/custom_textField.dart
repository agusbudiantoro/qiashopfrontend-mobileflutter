import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextFieldLogin extends StatefulWidget {
  final String hintText;
  TextEditingController isiField = TextEditingController();
  final IconData prefixIcon;
  final IconData suffixIcon;
  final bool isObsercure;
  final VoidCallback clickCallback;
  bool typeInput;
  bool typePass;

  TextFieldLogin(
      {this.hintText,
      this.isObsercure = false,
      this.prefixIcon,
      this.suffixIcon, this.clickCallback, this.isiField, this.typeInput, this.typePass});
  @override
  _TextFieldLoginState createState() => _TextFieldLoginState();
}

class _TextFieldLoginState extends State<TextFieldLogin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
          border: Border.all(width: 2.0, color: Colors.white),
          borderRadius: BorderRadius.all(Radius.circular(30.0))),
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: [
          Icon(
            widget.prefixIcon,
            color: Colors.white,
          ),
          SizedBox(width: 5.0),
          Expanded(
              child: TextField(
                minLines: 1,
                maxLines: (widget.typePass == true)?1:5, // jika type password maka maxlines == 1, jikan bukan password maka maxlines bisa 5
                keyboardType: (widget.typeInput == true)?TextInputType.number:TextInputType.multiline,
                controller: widget.isiField,
            obscureText: widget.isObsercure,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
            decoration: InputDecoration(
                hintText: widget.hintText,
                hintStyle: TextStyle(
                    color: Colors.white.withOpacity(0.8), fontSize: 18.0),
                border: InputBorder.none),
          )),
          SizedBox(width: 5),
          GestureDetector(
              onTap: () {
                widget.clickCallback();
              },
              child: widget.suffixIcon != null
                  ? Icon(
                      widget.suffixIcon,
                      color: Colors.white,
                    )
                  : Container())
        ],
      ),
    );
  }
}
