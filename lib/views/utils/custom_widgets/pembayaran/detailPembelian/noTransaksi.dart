import 'package:flutter/material.dart';

class PageNoTransaksi extends StatefulWidget {
  final String noTransaksi;
  final double width;
  PageNoTransaksi({this.noTransaksi, this.width});
  @override
  _PageNoTransaksiState createState() => _PageNoTransaksiState();
}

class _PageNoTransaksiState extends State<PageNoTransaksi> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(15),
        color: Colors.white,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "NO TRANSAKSI",
                  style: TextStyle(color: Colors.grey, fontSize: 10),
                ),
                Text(
                  widget.noTransaksi.toString(),
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            Container(
                        padding: EdgeInsets.only(top:15, bottom: 10),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      )
          ],
        ));
  }
}
