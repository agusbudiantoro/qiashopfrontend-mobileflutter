import 'package:flutter/material.dart';
import 'package:qiaShop/models/barang/model_barang.dart';

class WidgetDaftarBelanja extends StatefulWidget {
  final double width;
  final double height;
  final BarangModel dataBarang;
  final VoidCallback clickCallbackPlus;
  final VoidCallback clickCallbackMin;

  WidgetDaftarBelanja({this.height, this.width, this.dataBarang, this.clickCallbackPlus, this.clickCallbackMin});
  @override
  _WidgetDaftarBelanjaState createState() => _WidgetDaftarBelanjaState();
}

class _WidgetDaftarBelanjaState extends State<WidgetDaftarBelanja> {
  int qty;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    qty = widget.dataBarang.qty;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(15),
      height: widget.height / 5.5,
      width: widget.width,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              "Daftar Belanja",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            height: widget.height / 9,
            width: widget.width,
            child: Row(
              children: [
                Container(
                    width: widget.width / 5,
                    height: widget.height / 5,
                    child: Column(
                      children: [
                        Container(
                          height: widget.height / 12,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: new Image.network(
                              'https://qiaminimarketjaksel.com/gambar/'+widget.dataBarang.image.toString(),
                              // scale: 2,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ],
                    )),
                Container(
                  padding: EdgeInsets.all(10),
                  width: (widget.width - 38) - (widget.width / 5),
                  height: widget.height,
                  // color: Colors.white,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Flexible(
                            child: Text(
                              widget.dataBarang.namaBarang,
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Text(
                              (int.parse(widget.dataBarang.diskon) != 0)
                                  ? "Rp "+widget.dataBarang.hargaPromo
                                  : "Rp "+widget.dataBarang.harga,
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Container(
                              color: Colors.grey[300],
                              child: Row(
                                children: [
                                  Container(
                                    height: 30,
                                    width: 30,
                                    alignment: Alignment.center,
                                    child: IconButton(
                                        icon: Icon(
                                          Icons.remove,
                                          size: 15,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            if(qty <= widget.dataBarang.stok && qty >1 ){
                                              qty -= 1;
                                            }
                                          });
                                          if(widget.clickCallbackMin != null){
                                            return widget.clickCallbackMin();
                                          }
                                        }),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    width: 20,
                                    height: 25,
                                    color: Colors.white,
                                    child: Text(
                                      qty.toString(),
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ),
                                  Container(
                                    height: 30,
                                    width: 30,
                                    alignment: Alignment.center,
                                    child: IconButton(
                                        icon: Icon(
                                          Icons.add,
                                          size: 15,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            if(qty < widget.dataBarang.stok && qty >=1 ){
                                            qty += 1;
                                            }
                                          });
                                          if(widget.clickCallbackPlus != null){
                                            return widget.clickCallbackPlus();
                                          }
                                        }),
                                  ),
                                ],
                              )),
                        ],
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Text(
                              (int.parse(widget.dataBarang.diskon) != 0)
                                  ? "Rp "+(int.parse(widget.dataBarang.hargaPromo) *qty).toString()
                                  : "Rp "+(int.parse(widget.dataBarang.harga) *qty).toString(),
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
