import 'package:flutter/material.dart';
import 'package:qiaShop/views/widgets/isiFeed/feed.dart';
import 'package:qiaShop/views/widgets/keranjang/backgroundKeranjang.dart';


class FeedPage extends StatefulWidget {
  final int jenis;

  FeedPage({this.jenis});
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      
      body: IsiFeed(jenis: widget.jenis,)
    );
  }
}
