import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:qiaShop/views/widgets/detailProduk/background.dart';

class DetailProduk extends StatefulWidget {
  final String images;
  final String harga;
  final String hargaPromo;
  final String diskon;
  final String namaBarang;
  final String deskripsi;
  final int qty;
  final int idBarang;

  DetailProduk({this.idBarang,this.images, this.diskon, this.harga, this.hargaPromo, this.namaBarang, this.deskripsi, this.qty});
  @override
  _DetailProdukState createState() => _DetailProdukState();
}

class _DetailProdukState extends State<DetailProduk> {
  @override
  void initState() { 
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen,
      body: BackgroundDetailProduk(idBarang: widget.idBarang,images: widget.images,namaBarang: widget.namaBarang,harga: widget.harga,deskripsi: widget.deskripsi,diskon: widget.diskon,qty: widget.qty,hargaPromo: widget.hargaPromo,),
    );
  }
}