import 'dart:io';

class BarangModel {
  int idBarang;
  String namaBarang;
  String harga;
  String hargaPromo;
  String deskripsi;
  int qty;
  String image;
  String diskon;
  bool statusCeklis;
  File path;
  String kategori;
  int totalHarga;
  int stok;
  int idKeranjang;

  BarangModel({this.idKeranjang,this.stok,this.totalHarga ,this.idBarang,this.deskripsi, this.harga, this.hargaPromo, this.namaBarang, this.qty, this.image, this.diskon, this.statusCeklis, this.path, this.kategori});
}