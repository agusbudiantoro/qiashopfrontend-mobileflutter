class MyDataFormLogin {
  String email;
  String password;
  String username;
  String alamat;
  String noHp;

  MyDataFormLogin(
      {this.email,
      this.password,
      this.username,
      this.alamat,
      this.noHp});

  MyDataFormLogin.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    username = json['username'];
    alamat = json['alamat'];
    noHp = json['noHp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['username'] = this.username;
    data['alamat'] = this.alamat;
    data['noHp'] = this.noHp;
    return data;
  }
}